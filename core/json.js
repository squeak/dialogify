var _ = require("underscore");
var $$ = require("squeak");
var dialogify = require("./_");
require("./input");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: display a dialog to fill in a json text
  ARGUMENTS: ({
    ?value: <object> « an object to stringify, not directly the json string »,
    ... see dialogify.input for any other additional options
  })
  RETURN: see dialogify.input
*/
dialogify.json = function (options) {

  var fullOptions = $$.defaults({
    dialogClass: "dialogify-json",
    disableEnterKeyValidation: true,
    input: {
      _recursiveOption: true,
      type: "json",
      value: options.value,
      unvalidatedAlertError: false,
    },
  }, options);

  return dialogify.input(fullOptions);

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = dialogify.json;
