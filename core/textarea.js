var _ = require("underscore");
var $$ = require("squeak");
var dialogify = require("./_");
require("./input");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: display a dialog to fill in a long text
  ARGUMENTS: ({
    ?value: <string> « text to edit »,
    ... see dialogify.input for any other additional options
  })
  RETURN: see dialogify.input
*/
dialogify.textarea = function (options) {

  var fullOptions = $$.defaults({
    dialogClass: "dialogify-textarea",
    disableEnterKeyValidation: true,
    input: {
      _recursiveOption: true,
      type: "textarea",
      value: options.value,
      inputClass: "dialogify-textarea-input",
    },
  }, options);

  return dialogify.input(fullOptions);

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = dialogify.textarea;
