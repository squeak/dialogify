var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");
var dialogify = require("./_");
require("./input");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: display a dialog to choose in a tickbox list
  ARGUMENTS: ({
    ?title: <string>,
    !choices: <string[]|{ !value: <string>, ?label: <string>, }[]> « list of possible choices »,
    ?value: « currently set value(s) »,
    !ok: <function(value <any>)>,
    ?cancel: <function()>,
    ... any other dialogify.input options
  })
  RETURN: see dialogify.input
*/
dialogify.ticklist = function (options) {

  //
  //                              MAKE FULL OPTIONS

  var fullOptions = $$.defaults({

    dialogClass: "dialogify-ticklist",

    input: {
      _recursiveOption: true,
      type: "tickbox",
      choices: options.choices,
      radioTickbox: {
        orientation: "vertical",
      },
      value: options.value,
    },

    buttons: function (butttons, allDefaultButtons) {
      return [

        // SELECT ALL
        {
          title: "Select <u>a</u>ll",
          key: "a", // 65
          // scope: "auxiliary", // implement id uify.dialog ??
          click: function () {
            dialogifyForm.inputified.set(options.choices);
            return false;
          },
        },

        // SELECT NONE
        {
          title: "Select <u>n</u>one",
          key: "n", // 78
          // scope: "auxiliary", // implement id uify.dialog ??
          click: function () {
            dialogifyForm.inputified.set([]);
            return false;
          },
        },

        // DEFAULT BUTTONS
        allDefaultButtons.cancel,
        allDefaultButtons.ok,

      ];
    },

  }, options);

  //
  //                              MAKE DIALOGIFY FORM

  var dialogifyForm = dialogify.input(fullOptions);
  return dialogifyForm;

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = dialogify.ticklist;
