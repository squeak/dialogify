var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");
require("uify/extension/differences");
var inputify = require("inputify");
var dialogify = require("./_");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: display a dialog to fill in a custom input (you can also make a full form using "object" input type)
  ARGUMENTS: ({
    ?title: <string>,
    ?structure: <inputify·options[]> « the list of inputs to display »,
    ?value: <object> « the value of the object before modification »,
    ?comment: <string> « a note to show before the input »,
    !ok: <function(value <any>)>,
    ?cancel: <function(value <any>)>@default=function(value){ ...confirm losing modifs... } « if you don't set any cancel method, the default cancel method asks to confirm closing if there was any modification »,
    ?dialogClass: <string> « class to apply to dialog »,
    ... any other uify.confirm options
  })
  RETURN: see dialogify.input
*/
dialogify.object = function (options) {

  // patch to make sure dialog class dialogify-object is always set
  if (!_.isUndefined(options.dialogClass)) options.dialogClass = "dialogify-object "+ options.dialogClass;

  var dialogified = dialogify.input($$.defaults({
    type: "object",
    dialogClass: "dialogify-object",
    input: {
      inputifyType: "object",
      value: options.value,
      inputClass: "dialogify-object-input",
      object: {
        structure: options.structure
      },
    },
    cancel: function (value) {

      // if is equal, just close dialog
      if ($$.isEqual(options.value, value)) return true;

      // else ask a confirmation
      uify.differences.confirm({
        text: "Close and lose modifications?",
        before: options.value,
        after: value,
        ok: function () { dialogified.destroy(); },
      });

      // return false to disable closing modal, will be closed by uify.confirm callback if necessary
      return false;

    },
  }, options));

  return dialogified;

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = dialogify.object;
