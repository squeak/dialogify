var _ = require("underscore");
var $$ = require("squeak");
var dialogify = require("./_");
require("./input");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: display a dialog to choose in a radio list
  ARGUMENTS: ({
    ?title: <string>,
    !choices: <string[]|{ !value: <string>, ?label: <string>, }[]> « list of possible choices »,
    ?value: « currently set value »,
    !ok: <function(value <any>)>,
    ?cancel: <function()>,
    ... any other dialogify.input options
  })
  RETURN: see dialogify.input
*/
dialogify.radio = function (options) {

  var fullOptions = $$.defaults({
    dialogClass: "dialogify-radio",
    input: {
      _recursiveOption: true,
      type: "radio",
      choices: options.choices,
      radioTickbox: {
        orientation: "vertical",
      },
      value: options.value,
    },
  }, options);

  return dialogify.input(fullOptions);

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = dialogify.radio;
