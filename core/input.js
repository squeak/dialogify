var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");
var inputify = require("inputify");
var dialogify = require("./_");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: display a dialog to fill in a custom input (you can also make a full form using "object" input type)
  ARGUMENTS: ({
    ?title: <string>,
    ?input: <inputify·options but without "name", "$container", "labelLayout" and "unvalidatedThrowError" keys>,
    ?comment: <string> « a note to show before the input »,
    !ok: <function(value <any>)>,
    ?cancel: <function(value <any>)>,
    ?dialogClass: <string> « class to apply to dialog »,
    ... any other uify.confirm options
  })
  RETURN: <uify.confirm·return>
*/
dialogify.input = function (options) {

  //
  //                              GET OPTIONS PASSED

  $$.mandatory(options, {
    input: _.isObject,
  });

  var dialogFormOptions = _.clone(options);
  dialogFormOptions.dialogClass = dialogFormOptions.dialogClass ? "dialogify-input "+ dialogFormOptions.dialogClass : "dialogify-input";

  //
  //                              MAKE DIALOG

  var formInputified;
  var dialog = uify.dialog($$.defaults(dialogFormOptions, {

    overlayClickExits: false,

    content: function ($container) {

      // display comment
      if (dialogFormOptions.comment) $container.div({
        class: "comment",
        htmlSanitized: dialogFormOptions.comment,
      });

      // set inputify options
      dialogFormOptions.input.$container = $container;
      dialogFormOptions.input.name = "result";
      dialogFormOptions.input.labelLayout = "hidden";
      dialogFormOptions.input.unvalidatedThrowError = true;

      // create input
      formInputified = inputify(dialogFormOptions.input);

    },

    ok: function () {
      if (dialog.$content.find(".inputify_container.invalid").length) {
        uify.alert.error("Some inputs are invalid.<br>Please correct them if you want to validate.");
        return false;
      }
      else if (dialog.$content.find(".inputify_container.mandatory_missing").length) {
        uify.alert.error("You didn't fill some mandatory inputs.<br>Please do it before validating.");
        return false;
      }
      else if (dialogFormOptions.ok) {
        var getResult;
        try { getResult = formInputified.get(); }
        catch (e) {
          $$.log.error(e);
          return false;
        }
        return dialogFormOptions.ok(getResult);
      };
    },

    cancel: function () {
      if (dialogFormOptions.cancel) {
        try { var inputValue = formInputified.get(); }
        catch (e) { $$.log.error(e); };
        return dialogFormOptions.cancel(inputValue);
      };
    },

  }));
  dialog.inputified = formInputified;

  //
  //                              RETURN DIALOG OBJECT

  return dialog;

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = dialogify.input;
