
var dialogify = require("./_");

require("./input");
require("./json");
require("./object");
require("./radio");
require("./textarea");
require("./ticklist");

module.exports = dialogify;
