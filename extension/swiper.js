var _ = require("underscore");
var $$ = require("squeak");
var $ = require("yquerj");
var uify = require("uify");
require("uify/extension/swiper");
var dialogify = require("../core/_");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: just like uify.swiper, but it will display in a modal
  ARGUMENTS: (
    see uify.swiper·options but without "$target"
    see uify.alert but without "content"
  )
  RETURN: <uify.alert·return>
*/
dialogify.swiper = function (options) {

  var dialogOptions = $$.defaults(options, {
    dialogClass: "dialogify-swiper"+ (options && options.dialogClass ? " "+ dialogClass : ""),
    content: function ($contentContainer, alertifySettings) {
      var fullOptions = $$.defaults({ $target: $contentContainer }, options);
      uify.swiper(fullOptions);
    },
  });

  return uify.alert(dialogOptions);

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = dialogify.swiper;
