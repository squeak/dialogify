var dialogify = require("../../../core");
require("../../../extension/swiper");
var uify = require("uify");
var inputify = require("inputify");

window.demoStart = function (utilities) {

  utilities.section({
    title: "dialogify",
    demo: function ($container) {
      // o            >            +            #            °            ·            ^            :            |

      var dialogs = {

        //
        //                              RADIO

        "dialogify.radio": function () {
          dialogify.radio({
            title: "radio choice dialog",
            choices: [ "Exosilo", "Iconobmilo", "Stachinomzilo", ],
            ok: function (value) { uify.toast.success("you chose: "+ value); },
            cancel: function (value) { alert("you canceled"); },
          });
        },

        //
        //                              TICKBOX

        "dialogify.ticklist": function () {
          dialogify.ticklist({
            title: "tickbox choice dialog",
            choices: [ "Exosilo", "Artreohilo", "Iconobmilo", "Stachinomzilo", "Steroanclomilo", ],
            ok: function (value) { alert("you chose:\n"+ $$.json.pretty(value)); },
            cancel: function (value) { if (!confirm("Cancel?")) return false; },
          });
        },

        //
        //                              OBJECT/FORM

        "dialogify.object": function () {

          dialogify.object({
            title: "form dialog",
            structure: [
              {
                type: "normal",
                name: "title",
              },
              {
                type: "simple",
                name: "author",
                choices: [ "Fyodor Dostoyevsky", "Leo Tolstoy", "Vasily Zhukovsky", "Yuri Rytkheu", "Alexander Afanasyev", "Leonid Andreyev", ],
              },
              {
                type: "radio",
                name: "how",
                choices: ["read", "listened"],
                defaultValue: "read",
              },
              {
                type: "long",
                name: "summary",
              }
            ],
            ok: function (value) { alert($$.json.pretty(value)); },
            cancel: function (value) { if (!confirm("Cancel?")) return false; },
          });

        },

        //
        //                              TEXTAREA

        "dialogify.textarea": function () {

          dialogify.textarea({
            value: "Some long\n\ntext\n\nyou can edit.",
          });

        },

        //
        //                              TEXTAREA

        "dialogify.json": function () {

          dialogify.json({
            comment: "The returned value will be the value interpretted with JSON.parse()",
            value: {
              some: "key",
              number: 8,
              val: [
                "Hello",
                "world",
              ],
            },
          });

        },

        //
        //                              INPUT

        "dialogify.input": function () {

          var inputType = $$.random.entry(_.keys(inputify.inputsList));
          dialogify.input({
            title: "input dialog",
            comment: "Type of this input has been chosen randomly, open dialog again it will be different one.",
            input: {
              type: inputType,
            },
            ok: function (value) { alert("Input type was: "+ inputType +"\nValue:\n"+ $$.json.pretty(value)); },
          });

        },

        //
        //                              SWIPER

        "dialogify.swiper": function () {

          dialogify.swiper({
            title: "swiper dialog",
            slides: ["uify", "electrode", "squyntax", "stylectrode", "yquerj", "squeak"],
            navigationArrows: true,
            scrollbar: true,
            slideMaker: function ($slider, slide, next) {
              $slider.div().css({
                backgroundImage: 'url("/share/logos/'+ slide +'.png")',
                marginTop: "10%",
                height: "70%",
                backgroundRepeat: "no-repeat",
                backgroundSize: "contain",
                backgroundPosition: "center",
              });
            },
          });

        },

        //                              ¬
        //

      };

      // o            >            +            #            °            ·            ^            :            |

      var buttonsList = _.map(dialogs, function (func, name) {
        return {
          title: name,
          inlineTitle: "right",
          click: func,
          css: {
            fontSize: "2em",
            display: "inline-block",
          },
        };
      });
      uify.buttons($container, buttonsList);

      // o            >            +            #            °            ·            ^            :            |
    },

  });

};
