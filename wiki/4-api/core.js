
function meth (methodName) {
  return {
    name: "dialogify."+ methodName,
    path: "core/"+ methodName,
  };
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {

  display: {
    title: "CORE METHODS",
    comment: "Core methods that are always available when you import dialogify.",
    borderColor: "primary",
  },

  methods: [
    meth("input"),
    meth("json"),
    meth("object"),
    meth("radio"),
    meth("textarea"),
    meth("ticklist"),
  ],

};
