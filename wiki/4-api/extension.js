const _ = require("underscore");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = {

  display: {
    title: "EXTENSIONS",
    comment: 'Extensions, you must import them with <pre style="display:inline">require("dialogify/extension/&ltextensionName&gt")</pre>.',
    borderColor: "secondary",
  },

  methods: [
    {
      name: "dialogify.swiper",
      path: "extension/swiper",
    },
  ],

};
