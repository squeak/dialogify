# Usage

Load and use dialogify library with:

```javascript
const dialogify = require("dialogify");
dialogify.radio({
  title: "Choose a fruit",
  choices: [ "melon", "pear", "banana", "orange", "kiwi" ],
  value: "pear",
  ok: function (chosenFruit) { alert("You chose "+ chosenFruit +".") },
});
```

## Extensions

Extensions are not provided by default when you import dialogify. To use some extension, you can either do:
```javascript
const dialogify = require("dialogify");
require("dialogify/extension/<extensionName>")
```
or directly:
```javascript
const someDialogifyExtension = require("dialogify/extension/<extensionName>")
```

You will also need to import styles:
```css
@import "dialogify/styles/index";
@import "dialogify/extension/<extensionName>";
```

## Can I create a dialog containing a form?

To do a form, just use dialogify.object ;)
