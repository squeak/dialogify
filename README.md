# dialogify

Small library to display more complex dialogs, like forms, prompts, photo galleries...

For the full documentation, installation instructions... check [dialogify documentation page](https://squeak.eauchat.org/libs/dialogify/).
